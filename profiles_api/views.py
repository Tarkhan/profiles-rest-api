from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, viewsets
from profiles_api import serializers
from profiles_api.models import UserProfile


class HelloApiView(APIView):
    """Test API View"""

    serializer_class = serializers.HelloSerializer

    def get(self, request, format=None):
        """Returns a list of APIView features"""

        an_apiview = [
            'Salam',
            'Necesen?',
            'Ne var ne yox?',
            'Vezyet necedi?',
        ]

        return Response({'message:': 'Hello!', 'an_apiview': an_apiview})

    def post(self, request):

        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            name = serializer.validated_data.get('name')
            message = f'Hello {name}'
            return Response({'message': message})
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk=None):
        "Handle updating object"
        return Response({'message': 'PUT'})

    def patch(self, request, pk=None):
        "Handle partial updating of object"
        return Response({'message': 'PATCH'})

    def delete(self, request, pk=None):
        "Delete object"
        return Response({'message': 'Delete'})


class HelloViewSet(viewsets.ViewSet):
    """Tests api viewset"""

    serializer_class = serializers.HelloSerializer

    def list(self, request):
        """Return hello message"""

        a_viewset = [
            'Salam',
            'Necesen?',
            'Ne var ne yox?',
            'Vezyet necedi?',
        ]

        return Response({'message': 'Hello!', 'a_viewset': a_viewset})

    def create(self, request):

        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            name = serializer.validated_data.get('name')
            message = f'Hello {name}'
            return Response({'message': message})
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk=None):
        """Handle getting object by its id"""

        return Response({'http_method': 'GET'})

    def update(self, request, pk=None):
        """Handle uptading object by its id"""

        return Response({'http_method': 'PUT'})

    def partial_update(self, request, pk=None):
        """Handle partially uptading object by its id"""

        return Response({'http_method': 'PATCH'})

    def destroy(self, request, pk=None):
        """Handle removing of object by its id"""

        return Response({'http_method': 'DELETE'})


class UserProfileViewSet(viewsets.ModelViewSet):
    """Handle creating and updating profiles"""

    serializer_class = serializers.UserProfileSerializer
    queryset = UserProfile.objects.all()